
import React from 'react';
import { StyleSheet, View, Text, TextInput, Button, Alert } from 'react-native';
import auth from '@react-native-firebase/auth';
import { GoogleSignin, statusCodes } from '@react-native-community/google-signin';
import { LoginButton,LoginManager, AccessToken } from 'react-native-fbsdk';

export default class LoginScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = { 
            email: "",
            password: "",
            user:{},
            initializing: true
        };
    }


    componentDidMount() {
        var that = this;
        auth().onAuthStateChanged(function(user){
            that.setState({user: user,initializing : false});
        })
        GoogleSignin.configure({
          webClientId: '770810642676-nu0mt2cbt09grl1u0ncc90vm1h18ir7a.apps.googleusercontent.com',
          offlineAccess: false
        })
      
      }

      onGoogleButtonPress = async () => {
        try {
          /*await GoogleSignin.hasPlayServices();
          const userInfo = await GoogleSignin.signIn();
          this.setState({ userInfo });
          console.log(userInfo);*/
          const { idToken } = await GoogleSignin.signIn();
        console.log("email is "+idToken.email);
          // Create a Google credential with the token
          const googleCredential = auth.GoogleAuthProvider.credential(idToken);
        
          // Sign-in the user with the credential
          return auth().signInWithCredential(googleCredential);
          
        } catch (error) {
          console.log(error);
          if (error.code === statusCodes.SIGN_IN_CANCELLED) {
            // user cancelled the login flow
          } else if (error.code === statusCodes.IN_PROGRESS) {
            // operation (e.g. sign in) is in progress already
          } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
            // play services not available or outdated
          } else {
            // some other error happened
          }
        }
    }
    
    onLoginPress = () => {
            auth()
            .signInWithEmailAndPassword(this.state.email, this.state.password)
            .then(() => {
              console.log('User account created & signed in!');
            })
            .catch(error => {
                Alert.alert(error.message); 
              if (error.code === 'auth/email-already-in-use') {
                console.log('That email address is already in use!');
              }
          
              if (error.code === 'auth/invalid-email') {
                console.log('That email address is invalid!');
              }
          
              console.error(error);
            });
    }

    onCreateAccountPress = () => {
        var navActions = NavigationActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({routeName: "Signup"})]
        });
        this.props.navigation.dispatch(navActions);
    }
    signOut = async () => {
      try {
       // await GoogleSignin.revokeAccess();
        await GoogleSignin.signOut();
        this.setState({ user: null }); // Remember to remove the user from your app's state as well
      } catch (error) {
        console.error(error);
      }
    };
    

    onFacebookButtonPress = async () => {
      // Attempt login with permissions
      const result = await LoginManager.logInWithPermissions(['public_profile', 'email']);
    
      if (result.isCancelled) {
        throw 'User cancelled the login process';
      }
    
      // Once signed in, get the users AccesToken
      const data = await AccessToken.getCurrentAccessToken();
    
      if (!data) {
        throw 'Something went wrong obtaining access token';
      }
    
      // Create a Firebase credential with the AccessToken
      const facebookCredential = auth.FacebookAuthProvider.credential(data.accessToken);
    
      // Sign-in the user with the credential
      return auth().signInWithCredential(facebookCredential);
    }
    onForgotPasswordPress = () => {
        var navActions = NavigationActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({routeName: "ForgotPassword"})]
        });
        this.props.navigation.dispatch(navActions);
    }

    render() {
        //const { navigation } = this.props;

        //if (this.state.initializing) return null;

        /*if (!this.state.user) {
            return (
                <View style={{paddingTop:50, alignItems:"center"}}>

                    <TextInput style={{width: 200, height: 40, borderWidth: 1}}
                        value={this.state.email}
                        onChangeText={(text) => { this.setState({email: text}) }}
                        placeholder="Email"
                        keyboardType="email-address"
                        autoCapitalize="none"
                        autoCorrect={false}
                    />
    
                    <View style={{paddingTop:10}} />
    
                    <TextInput style={{width: 200, height: 40, borderWidth: 1}}
                        value={this.state.password}
                        onChangeText={(text) => { this.setState({password: text}) }}
                        placeholder="Password"
                        secureTextEntry={true}
                        autoCapitalize="none"
                        autoCorrect={false}
                    />
    
                    <Button title="Login" onPress={this.onLoginPress} />
                    <Button title="Create account..." onPress={() => navigation.navigate('SignupScreen')} />
                    <Button title="Forgot Password..." onPress={this.onForgotPasswordPress} />
                    <Button title="Signin with google" onPress={() => this.onGoogleButtonPress().then(() => console.log('Signed in with Google!'))} />
                </View>
            );
        }*/ 
        return (
            <View style={styles.container}>
            <Text>Welcome {this.state.user && this.state.user.email ? this.state.user.email : 'not signin'}</Text>
            <Button title="Signin with google" onPress={() => this.onGoogleButtonPress().then(() => console.log('Signed in with Google!'))} />
            <Button title="Logout" onPress={this.signOut} />
            <View>
            <Button title="Facebook Sign-In" onPress={() =>this.onFacebookButtonPress().then(() => console.log('Signed in with Facebook!'))} />
      </View>
           </View>
           
          ); 
              

    }
}

const styles = StyleSheet.create({
  container: {
    marginTop: Platform.OS === 'ios' ? 40 : 0
  }
});